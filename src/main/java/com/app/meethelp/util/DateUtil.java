package com.app.meethelp.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static Date getDateFromParameter(String dateParam){
        Date date = new Date();
        date.setDate(Integer.valueOf(dateParam.substring(0, 2)));
        date.setMonth(Integer.valueOf(dateParam.substring(3, 5)) - 1);
        date.setYear(Integer.valueOf(dateParam.substring(6, 10)) - 1900);
        return date;
    }
}
