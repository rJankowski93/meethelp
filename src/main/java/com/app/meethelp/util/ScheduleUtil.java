 package com.app.meethelp.util;

import com.app.meethelp.dao.ScheduleDayDAO;
import com.app.meethelp.data.ScheduleDayData;

import java.sql.Time;
import java.util.*;

public class ScheduleUtil {
    public static List<ScheduleDayData> getFreeSchedules(List<ScheduleDayData> schedules, Date date, String userName) {
        List<ScheduleDayData> freeSchedules = new ArrayList<>();
        if(schedules.isEmpty()){
            freeSchedules.add(new ScheduleDayData("Wolna przerwa " + 1, date, new Time(0,0,0),new Time(23,59,59),userName, true));
        }
        else {
            Time tempTime = schedules.get(0).getDateTo();
            int lastIndexBreak = 0;
            int freeSpaceNumber = 1;
            for (int i = 1; i < schedules.size(); i++) {
                if (schedules.get(i).getDateFrom().after(tempTime)) {
                    freeSchedules.add(new ScheduleDayData("Wolna przerwa " + freeSpaceNumber, date, tempTime, schedules.get(i).getDateFrom(), userName, true));
                    lastIndexBreak = i;
                    freeSpaceNumber++;
                }
                tempTime = schedules.get(i).getDateTo();
            }
            for (ScheduleDayData schedule : schedules) {
                if (schedule.getDateTo().after(tempTime)) {
                    tempTime = schedule.getDateTo();
                }
            }
            freeSchedules.add(new ScheduleDayData("Wolna przerwa " + freeSpaceNumber, date, tempTime, new Time(0, 0, 0), userName, true));
        }
        return freeSchedules;
    }

    public static List<ScheduleDayData> getSchedulesFromParams( Map<String, String[]> parameters, Date date, String userName){
        ScheduleDayData schedule = null;
        List<ScheduleDayData> schedules = new ArrayList<>();
        for (Map.Entry<String, String[]> entry : parameters.entrySet()) {
            if (entry.getKey().contains("timeFrom")) {
                schedule = new ScheduleDayData();
                schedule.setDay(date);
                schedule.setDateFrom(new Time(Integer.valueOf(entry.getValue()[0].substring(0, 2)), Integer.valueOf(entry.getValue()[0].substring(3, 5)), 0));
                schedule.setName(entry.getKey().substring(8));
                schedule.setUserName(userName);
            } else if (entry.getKey().contains("timeTo")) {
                schedule.setDateTo(new Time(Integer.valueOf(entry.getValue()[0].substring(0, 2)), Integer.valueOf(entry.getValue()[0].substring(3, 5)), 0));
                ScheduleDayDAO.getInstance().addScheduleDay(schedule);
                schedules.add(schedule);
            }
        }
        Collections.sort(schedules, new ScheduleDayData());
        return schedules;
    }
}
