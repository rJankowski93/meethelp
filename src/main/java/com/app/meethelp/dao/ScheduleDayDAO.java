package com.app.meethelp.dao;

import com.app.meethelp.data.ScheduleDayData;
import com.app.meethelp.db.ConnectorDB;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.Date;
import java.util.List;

public class ScheduleDayDAO {

    private static ScheduleDayDAO instance;

    private ConnectorDB connectorDB;

    public static final ScheduleDayDAO getInstance() {
        if (instance == null) {
            synchronized (ScheduleDayDAO.class) {
                if (instance == null) {
                    instance = new ScheduleDayDAO();
                }
            }
        }
        return instance;
    }

    private ScheduleDayDAO() {
        connectorDB = ConnectorDB.getInstance();
    }

    public void addScheduleDay(ScheduleDayData scheduleDay) {
        Transaction transaction = null;
        if (connectorDB.session.getTransaction().isActive()) {
            transaction = connectorDB.session.getTransaction();
        } else {
            transaction = connectorDB.session.beginTransaction();
        }
        connectorDB.session.save(scheduleDay);
        transaction.commit();
    }

    public ScheduleDayData getScheduleDay(String name, Date day, String userName, boolean free) {
        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //String date = format.parse(day);
        String textQuery = "from ScheduleDayData where name = :name AND day = :day AND userName = :userName AND free = :free";
        Query query = connectorDB.session.createQuery(textQuery);
        query.setParameter("name", name);
        query.setTimestamp("day", day);
        query.setParameter("userName", userName);
        query.setParameter("free", free);
        List list = query.list();
        if (list.isEmpty()) {
            return null;
        } else {
            return (ScheduleDayData) list.get(0);
        }
    }

    //TODO for define user
    public List<ScheduleDayData> getSchedules(Date day, String userName, boolean free) {
        String textQuery = "from ScheduleDayData where day = :day AND userName = :userName AND free = :free order by dateFrom";
        Query query = connectorDB.session.createQuery(textQuery);
        query.setTimestamp("day", day);
        query.setParameter("userName", userName);
        query.setParameter("free", free);
        return query.getResultList();
    }

    public void modifyScheduleDay(ScheduleDayData scheduleDay) {
        //modify scheduleDay to dao
    }
}
