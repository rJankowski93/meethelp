package com.app.meethelp.dao;

import com.app.meethelp.dto.TimetableDTO;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TimetableDAO {

    private static TimetableDAO instance;

    public static final TimetableDAO getInstance() {
        if (instance == null) {
            synchronized (TimetableDAO.class) {
                if (instance == null) {
                    instance = new TimetableDAO();
                }
            }
        }
        return instance;
    }

    private TimetableDAO() {
    }

    public List<TimetableDTO> readTimetablePKP(String startPlace, String endPlace, int year, int month, int day, int hour, int minute, Date date) throws IOException {
        String url = getPkpUrl(startPlace, endPlace, (year - 100), (month + 1), day, hour, minute);
        String textHtml = getHtml(url);
        List<TimetableDTO> timetables = new ArrayList<TimetableDTO>();
        int iterator = textHtml.indexOf("przesiadki:");
        while (iterator >= 0) {
            TimetableDTO timetable = new TimetableDTO();
            timetable.setNumberOfChanges(textHtml.substring(iterator + 12, iterator + 13));
            iterator = textHtml.indexOf("czas przejazdu:", iterator + 1);
            timetable.setDurationTrip(textHtml.substring(iterator + 15, iterator + 19));
            iterator = textHtml.indexOf("ODJAZD</span><span>", iterator + 1);
            timetable.setDepartures(textHtml.substring(iterator + 19, iterator + 24));
            iterator = textHtml.indexOf("PRZYJAZD</span><span>", iterator + 1);
            timetable.setArrivals(textHtml.substring(iterator + 21, iterator + 26));
            iterator = textHtml.indexOf("przesiadki:", iterator + 1);
            timetable.setStartPlace(startPlace);
            timetable.setEndPlace(endPlace);
            timetable.setDate(date);
            timetables.add(timetable);
        }
        return timetables;
    }

    public void readTimetableMZK(int day, int hour, int minute) throws IOException {
        String url = getMzkUrl(day, hour, minute);
        String textHtml = getHtml(url);
        //TODO read mzkUrl
    }

    private String getPkpUrl(String startPlace, String endPlace, int year, int month, int day, int hour, int minute) {
        String pkpUrl = "";
        if (startPlace.equals("Wroclaw") && endPlace.equals("Katowice")) {
            pkpUrl = "http://rozklad-pkp.pl/pl/tp?queryPageDisplayed=yes&REQ0JourneyStopsS0A=1&REQ0JourneyStopsS0G=5100069&REQ0JourneyStopsS0ID=&REQ0Journey" +
                    "Stops1.0G=&REQ0JourneyStopover1=&REQ0JourneyStops2.0G=&REQ0JourneyStopover2=&REQ0JourneyStopsZ0A=1&REQ0JourneyStopsZ0G=5100020&REQ0" +
                    "JourneyStopsZ0ID=&date=" + day + "." + month + "." + year + "&dateStart=" + day + "." + month + "." + year + "&dateEnd=" + day + "." + month + "." + year + "&" +
                    "REQ0JourneyDate=" + day + "." + month + "." + year + "&time=" + hour + "%3A" + minute + "&REQ0JourneyTime=" + hour + "%3A" + minute + "&REQ0Hafas" +
                    "SearchForw=1&existBikeEverywhere=yes&existHafasAttrInc=yes&existHafasAttrInc=yes&REQ0JourneyProduct_prod_section_0_0=1&REQ0Journey" +
                    "Product_prod_section_1_0=1&REQ0JourneyProduct_prod_section_2_0=1&REQ0JourneyProduct_prod_section_3_0=1&REQ0JourneyProduct_prod_section_0_1=1&" +
                    "REQ0JourneyProduct_prod_section_1_1=1&REQ0JourneyProduct_prod_section_2_1=1&REQ0JourneyProduct_prod_section_3_1=1&REQ0JourneyProduct_prod_" +
                    "section_0_2=1&REQ0JourneyProduct_prod_section_1_2=1&REQ0JourneyProduct_prod_section_2_2=1&REQ0JourneyProduct_prod_section_3_2=1&REQ0" +
                    "JourneyProduct_prod_section_0_3=1&REQ0JourneyProduct_prod_section_1_3=1&REQ0JourneyProduct_prod_section_2_3=1&REQ0JourneyProduct_prod_" +
                    "section_3_3=1&REQ0JourneyProduct_opt_section_0_list=0%3A000000&existOptimizePrice=1&existHafasAttrExc=yes&REQ0HafasChangeTime=0%3A1&exist" +
                    "SkipLongChanges=0&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttr" +
                    "Exc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&existHafasAttrInc=yes&existHafasAttrExc=" +
                    "yes&wDayExt0=Pn%7CWt%7C%C5%9Ar%7CCz%7CPt%7CSo%7CNd&start=start&existUnsharpSearch=yes&came_from_form=1#focus";
        } else if (startPlace.equals("Katowice") && endPlace.equals("Tychy")) {
            pkpUrl = "http://rozklad-pkp.pl/pl/tp?queryPageDisplayed=yes&REQ0JourneyStopsS0A=1&REQ0JourneyStopsS0G=5100020&REQ0JourneyStopsS0ID=&REQ0Journey" +
                    "Stops1.0G=&REQ0JourneyStopover1=&REQ0JourneyStops2.0G=&REQ0JourneyStopover2=&REQ0JourneyStopsZ0A=1&REQ0JourneyStopsZ0G=5100276&REQ0" +
                    "JourneyStopsZ0ID=&date=" + day + "." + month + "." + year + "&dateStart=30.10.16&dateEnd=" + day + "." + month + "." + year + "&REQ0Journey" +
                    "Date=" + day + "." + month + "." + year + "&time=" + hour + "%3A" + minute + "&" + "REQ0JourneyTime=" + hour + "%3A" + minute + "&" +
                    "REQ0HafasSearchForw=1&existBikeEverywhere=yes&existHafasAttrInc=yes&existHafasAttrInc=yes&REQ0JourneyProduct_prod_section_0_0=1&REQ0" +
                    "JourneyProduct_prod_section_1_0=1&REQ0JourneyProduct_prod_section_2_0=1&REQ0JourneyProduct_prod_section_3_0=1&REQ0JourneyProduct" +
                    "_prod_section_0_1=1&REQ0JourneyProduct_prod_section_1_1=1&REQ0JourneyProduct_prod_section_2_1=1&REQ0JourneyProduct_prod_" +
                    "section_3_1=1&REQ0JourneyProduct_prod_section_0_2=1&REQ0JourneyProduct_prod_section_1_2=1&REQ0JourneyProduct_prod_section_2_2=1&" +
                    "REQ0JourneyProduct_prod_section_3_2=1&REQ0JourneyProduct_prod_section_0_3=1&REQ0JourneyProduct_prod_section_1_3=1&REQ0Journey" +
                    "Product_prod_section_2_3=1&REQ0JourneyProduct_prod_section_3_3=1&REQ0JourneyProduct_opt_section_0_list=0%3A000000&existOptimize" +
                    "Price=1&existHafasAttrExc=yes&REQ0HafasChangeTime=0%3A1&existSkipLongChanges=0&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttr" +
                    "Exc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttr" +
                    "Exc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&existHafasAttrInc=yes&existHafasAttrExc=yes&wDayExt0=Pn%7CWt%7C%C5%9Ar%7CCz%7CPt%7CSo%7" +
                    "CNd&start=start&existUnsharpSearch=yes&came_from_form=1#focus";
        } else if (startPlace.equals("Tychy") && endPlace.equals("Wroclaw")) {
            pkpUrl = "http://rozklad-pkp.pl/pl/tp?queryPageDisplayed=yes&REQ0JourneyStopsS0A=1&REQ0JourneyStopsS0G=5100260&REQ0JourneyStopsS0ID=&REQ0" +
                    "JourneyStops1.0G=&REQ0JourneyStopover1=&REQ0JourneyStops2.0G=&REQ0JourneyStopover2=&REQ0JourneyStopsZ0A=1&REQ0JourneyStopsZ0G=5100069&REQ0" +
                    "JourneyStopsZ0ID=&date=" + day + "." + month + "." + year + "&dateStart=" + day + "." + month + "." + year + "&dateEnd=" + day + "." + month + "." + year + "&" +
                    "REQ0JourneyDate=" + day + "." + month + "." + year + "&time=" + hour + "%3A" + minute + "&REQ0JourneyTime=" + hour + "%3A" + minute + "&REQ0Hafas" +
                    "SearchForw=1&existBikeEverywhere=yes&existHafasAttrInc=yes&existHafasAttrInc=yes&REQ0JourneyProduct_prod_section_0_0=1&REQ0Journey" +
                    "Product_prod_section_1_0=1&REQ0JourneyProduct_prod_section_2_0=1&REQ0JourneyProduct_prod_section_3_0=1&REQ0JourneyProduct_prod_section_0_1" +
                    "=1&REQ0JourneyProduct_prod_section_1_1=1&REQ0JourneyProduct_prod_section_2_1=1&REQ0JourneyProduct_prod_section_3_1=1&REQ0Journey" +
                    "Product_prod_section_0_2=1&REQ0JourneyProduct_prod_section_1_2=1&REQ0JourneyProduct_prod_section_2_2=1&REQ0JourneyProduct_prod_" +
                    "section_3_2=1&REQ0JourneyProduct_prod_section_0_3=1&REQ0JourneyProduct_prod_section_1_3=1&REQ0JourneyProduct_prod_section_2_3=1&REQ0" +
                    "JourneyProduct_prod_section_3_3=1&REQ0JourneyProduct_opt_section_0_list=0%3A000000&existOptimizePrice=1&existHafasAttrExc=yes&REQ0Hafas" +
                    "ChangeTime=0%3A1&existSkipLongChanges=0&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0" +
                    "HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&REQ0HafasAttrExc=&existHafasAttr" +
                    "Inc=yes&existHafasAttrExc=yes&wDayExt0=Pn%7CWt%7C%C5%9Ar%7CCz%7CPt%7CSo%7CNd&start=start&existUnsharpSearch=yes&came_from_form=1#focus";
        }
        return pkpUrl;
    }

    private String getMzkUrl(int day, int hour, int minute) {
        String mzkUrl = "";
        //TODO set url
        return mzkUrl;
    }

    private String getHtml(String url) throws IOException {
        InputStream inputStream = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
        String textHtml = readAll(bufferedReader);
        return textHtml;
    }

    private String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

}
