package com.app.meethelp.dao;

import com.app.meethelp.data.UserData;
import com.app.meethelp.db.ConnectorDB;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class UserDAO {

    private static UserDAO instance;

    private ConnectorDB connectorDB;

    public static final UserDAO getInstance() {
        if (instance == null) {
            synchronized (UserDAO.class) {
                if (instance == null) {
                    instance = new UserDAO();
                }
            }
        }
        return instance;
    }

    private UserDAO() {
        connectorDB = ConnectorDB.getInstance();
    }

    public boolean checkUser(UserData user) {
        String textQuery = "from UserData where name = :name AND password = :password";
        Query query = connectorDB.session.createQuery(textQuery);
        query.setParameter("name", user.getName());
        query.setParameter("password", user.getPassword());
        if (query.list().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public void modifyUser(UserData user) {
        Transaction transaction= null;
        if(connectorDB.session.getTransaction().isActive()){
            transaction = connectorDB.session.getTransaction();
        }
        else{
            transaction = connectorDB.session.beginTransaction();
        }
        connectorDB.session.persist(user);
        transaction.commit();
    }

    public void addUser(UserData user) {
        Transaction transaction= null;
        if(connectorDB.session.getTransaction().isActive()){
            transaction = connectorDB.session.getTransaction();
        }
        else{
            transaction = connectorDB.session.beginTransaction();
        }
        connectorDB.session.save(user);
        transaction.commit();
    }
}
