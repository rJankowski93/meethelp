package com.app.meethelp.data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Comparator;
import java.util.Date;

@Entity
@Table(name = "SCHEDULE_DAY")
public class ScheduleDayData implements Serializable, Comparator<ScheduleDayData> {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DAY")
    private Date day;

    @Column(name = "DATE_FROM")
    private Time dateFrom;

    @Column(name = "DATE_TO")
    private Time dateTo;

    @Column(name = "FREE")
    private boolean free;

    @Column(name = "USER_NAME")
    private String userName;

    public ScheduleDayData() {
    }

    public ScheduleDayData(String name, Date day, String userName,boolean free) {
        this.name = name;
        this.day = day;
        this.free = free;
        this.userName = userName;
    }

    public ScheduleDayData(String name, Date day, Time dateFrom, Time dateTo, String userName ,boolean free) {
        this.name = name;
        this.day = day;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.free = free;
        this.userName = userName;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Time getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Time dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Time getDateTo() {
        return dateTo;
    }

    public void setDateTo(Time dateTo) {
        this.dateTo = dateTo;
    }

    public Boolean getFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public int compare(ScheduleDayData o1, ScheduleDayData o2) {
        return o1.getDateFrom().compareTo(o2.getDateFrom());
    }
}
