package com.app.meethelp.servlet;

import com.app.meethelp.dao.ScheduleDayDAO;
import com.app.meethelp.data.ScheduleDayData;
import com.app.meethelp.util.DateUtil;
import com.app.meethelp.util.ScheduleUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FreeWeekendForBothServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dateParam = request.getParameter("date");

        String day = dateParam.substring(0, 2);
        String mounth = dateParam.substring(3, 5);
        String year = dateParam.substring(6, 10);

        Date friday = new Date(Integer.valueOf(year) - 1900, Integer.valueOf(mounth) - 1, Integer.valueOf(day));
        Date saturday = DateUtil.addDays(friday, 1);
        Date sunday = DateUtil.addDays(friday, 2);

        List<ScheduleDayData> schedulesFridayPaulina = ScheduleDayDAO.getInstance().getSchedules(friday,"Paulina",false);
        List<ScheduleDayData> schedulesSaturdayPaulina = ScheduleDayDAO.getInstance().getSchedules(saturday,"Paulina",false);
        List<ScheduleDayData> schedulesSundayPaulina = ScheduleDayDAO.getInstance().getSchedules(sunday,"Paulina",false);

        List<ScheduleDayData> schedulesFridayRafal = ScheduleDayDAO.getInstance().getSchedules(friday,"Rafal",false);
        List<ScheduleDayData> schedulesSaturdayRafal = ScheduleDayDAO.getInstance().getSchedules(saturday,"Rafal",false);
        List<ScheduleDayData> schedulesSundayRafal = ScheduleDayDAO.getInstance().getSchedules(sunday,"Rafal",false);

        List<ScheduleDayData> schedulesFriday = Stream.concat(schedulesFridayPaulina.stream(), schedulesFridayRafal.stream()).collect(Collectors.toList());
        List<ScheduleDayData> schedulesSaturday = Stream.concat(schedulesSaturdayPaulina.stream(), schedulesSaturdayRafal.stream()).collect(Collectors.toList());
        List<ScheduleDayData> schedulesSunday = Stream.concat(schedulesSundayPaulina.stream(), schedulesSundayRafal.stream()).collect(Collectors.toList());

        Collections.sort(schedulesFriday, new ScheduleDayData());
        Collections.sort(schedulesSaturday, new ScheduleDayData());
        Collections.sort(schedulesSunday, new ScheduleDayData());

        List<ScheduleDayData> freeSchedulesFriday = ScheduleUtil.getFreeSchedules(schedulesFriday, friday, "");
        List<ScheduleDayData> freeSchedulesSaturday = ScheduleUtil.getFreeSchedules(schedulesSaturday, saturday, "");
        List<ScheduleDayData> freeSchedulesSunday = ScheduleUtil.getFreeSchedules(schedulesSunday, sunday, "");

        request.setAttribute("schedulesFriday", schedulesFriday);
        request.setAttribute("schedulesSaturday", schedulesSaturday);
        request.setAttribute("schedulesSunday", schedulesSunday);

        request.setAttribute("freeSchedulesFriday", freeSchedulesFriday);
        request.setAttribute("freeSchedulesSaturday", freeSchedulesSaturday);
        request.setAttribute("freeSchedulesSunday", freeSchedulesSunday);

        RequestDispatcher view = request.getRequestDispatcher("WEB-INF/view/FreeTimeWeekendForBoth.jsp");
        view.forward(request, response);
    }
}
