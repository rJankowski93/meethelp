package com.app.meethelp.servlet;

import com.app.meethelp.dao.UserDAO;
import com.app.meethelp.data.UserData;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        if(!request.getParameterMap().isEmpty()){
            UserData user = new UserData(request.getParameter("name"), request.getParameter("password"));
            if (UserDAO.getInstance().checkUser(user)) {
                synchronized (session){
                    session.setAttribute("userName", user.getName());
                }
                RequestDispatcher view = request.getRequestDispatcher("WEB-INF/view/ChooseOption.jsp");
                view.forward(request, response);
            }
            else{
                throw new RuntimeException("Wrong login or password");
            }
        }
        else if(session.getAttribute("userName") != null){
            RequestDispatcher view = request.getRequestDispatcher("WEB-INF/view/ChooseOption.jsp");
            view.forward(request, response);
        }
        else{
            throw new RuntimeException("Wrong login or password");
        }
    }
}
