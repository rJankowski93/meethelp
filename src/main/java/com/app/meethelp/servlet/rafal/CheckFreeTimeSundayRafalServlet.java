package com.app.meethelp.servlet.rafal;

import com.app.meethelp.dao.ScheduleDayDAO;
import com.app.meethelp.data.ScheduleDayData;
import com.app.meethelp.util.DateUtil;
import com.app.meethelp.util.ScheduleUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

public class CheckFreeTimeSundayRafalServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userName = (String) session.getAttribute("userName");
        String dateFridayParam = (String) session.getAttribute("dateFriday");

        Date dateFriday = new Date();
        dateFriday.setDate(Integer.valueOf(dateFridayParam.substring(0, 2)));
        dateFriday.setMonth(Integer.valueOf(dateFridayParam.substring(3, 5)) - 1);
        dateFriday.setYear(Integer.valueOf(dateFridayParam.substring(6, 10)) - 1900);
        Date dateSunday = DateUtil.addDays(dateFriday, 2);

        Map<String, String[]> parameters = new LinkedHashMap<>(request.getParameterMap());

        List<ScheduleDayData> schedules = ScheduleUtil.getSchedulesFromParams(parameters, dateSunday, userName);
        Collections.sort(schedules, new ScheduleDayData());

        List<ScheduleDayData> freeSchedules = ScheduleUtil.getFreeSchedules(schedules, dateSunday, userName);

        for (ScheduleDayData scheduleDay : schedules) {
            ScheduleDayDAO.getInstance().addScheduleDay(scheduleDay);
        }
        for (ScheduleDayData scheduleDay : freeSchedules) {
            ScheduleDayDAO.getInstance().addScheduleDay(scheduleDay);
        }


        ServletContext context= getServletContext();
        RequestDispatcher rd= context.getRequestDispatcher("/FreeWeekend.do");
        rd.forward(request, response);

//        RequestDispatcher view = request.getRequestDispatcher("WEB-INF/view/FreeTimeWeekend.jsp");
//        view.forward(request, response);
    }
}
