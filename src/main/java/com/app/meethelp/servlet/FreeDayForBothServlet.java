package com.app.meethelp.servlet;

import com.app.meethelp.dao.ScheduleDayDAO;
import com.app.meethelp.data.ScheduleDayData;
import com.app.meethelp.util.ScheduleUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FreeDayForBothServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dateParam = request.getParameter("date");

        String day = dateParam.substring(0, 2);
        String mounth = dateParam.substring(3, 5);
        String year = dateParam.substring(6, 10);

        Date today = new Date(Integer.valueOf(year) - 1900, Integer.valueOf(mounth) - 1, Integer.valueOf(day));

        List<ScheduleDayData> schedulesPaulina = ScheduleDayDAO.getInstance().getSchedules(today, "Paulina", false);
        List<ScheduleDayData> schedulesRafal = ScheduleDayDAO.getInstance().getSchedules(today, "Rafal", false);
        List<ScheduleDayData> schedules = Stream.concat(schedulesPaulina.stream(), schedulesRafal.stream()).collect(Collectors.toList());
        Collections.sort(schedules, new ScheduleDayData());

        List<ScheduleDayData> freeSchedules = ScheduleUtil.getFreeSchedules(schedules, today, "");

        request.setAttribute("schedules", schedules);
        request.setAttribute("freeSchedules", freeSchedules);
        RequestDispatcher view = request.getRequestDispatcher("WEB-INF/view/FreeTimeDayForBoth.jsp");
        view.forward(request, response);
    }
}
