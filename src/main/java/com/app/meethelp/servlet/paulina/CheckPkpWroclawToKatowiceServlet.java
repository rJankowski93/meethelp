package com.app.meethelp.servlet.paulina;

import com.app.meethelp.dao.ScheduleDayDAO;
import com.app.meethelp.dao.TimetableDAO;
import com.app.meethelp.data.ScheduleDayData;
import com.app.meethelp.dto.TimetableDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Time;
import java.util.Date;
import java.util.List;

public class CheckPkpWroclawToKatowiceServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dateParam = request.getParameter("date");
        String timeUniversityParam = request.getParameter("timeUniversity");
        String startSchoolParam = request.getParameter("startSchool");
        String endSchoolParam = request.getParameter("endSchool");

        HttpSession session = request.getSession();
        session.setAttribute("dateFriday", dateParam);

        Date date = new Date();
        date.setDate(Integer.valueOf(dateParam.substring(0, 2)));
        date.setMonth(Integer.valueOf(dateParam.substring(3, 5)) - 1);
        date.setYear(Integer.valueOf(dateParam.substring(6, 10)) - 1900);

        Time timeUniversity = new Time(Integer.valueOf(timeUniversityParam.substring(0, 2)), Integer.valueOf(timeUniversityParam.substring(3, 5)), 0);
        Time startSchool = new Time(Integer.valueOf(startSchoolParam.substring(0, 2)), Integer.valueOf(startSchoolParam.substring(3, 5)), 0);
        Time endSchool = new Time(Integer.valueOf(endSchoolParam.substring(0, 2)), Integer.valueOf(endSchoolParam.substring(3, 5)), 0);


        ScheduleDayDAO.getInstance().addScheduleDay(new ScheduleDayData("Uczelnia", date, new Time(0, 0, 0), timeUniversity, (String)session.getAttribute("userName"), false));
        ScheduleDayDAO.getInstance().addScheduleDay(new ScheduleDayData("Szkola", date, startSchool, endSchool,(String)session.getAttribute("userName"), false));

        List<TimetableDTO> timetableWroToKato = TimetableDAO.getInstance().readTimetablePKP("Wroclaw", "Katowice", date.getYear(), date.getMonth(), date.getDate(), timeUniversity.getHours(), timeUniversity.getMinutes(), date);

        request.setAttribute("timetableWroToKato", timetableWroToKato);
        RequestDispatcher view = request.getRequestDispatcher("WEB-INF/view/paulina/ChooseWroclawToKatowice.jsp");
        view.forward(request, response);
    }
}
