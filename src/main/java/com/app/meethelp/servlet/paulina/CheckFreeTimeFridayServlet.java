package com.app.meethelp.servlet.paulina;

import com.app.meethelp.dao.ScheduleDayDAO;
import com.app.meethelp.data.ScheduleDayData;
import com.app.meethelp.util.ScheduleUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CheckFreeTimeFridayServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String timetable = request.getParameter("timetable");
        String departure = timetable.substring(timetable.indexOf("Odjazd:") + 7, timetable.indexOf("Odjazd:") + 12);
        String arrival = timetable.substring(timetable.indexOf("Przyjazd:") + 9, timetable.indexOf("Przyjazd:") + 14);
        String time = timetable.substring(timetable.indexOf("Czas:") + 5, timetable.indexOf("Czas:") + 9);
        String changes = timetable.substring(timetable.indexOf("Przesiadki:") + 11, timetable.indexOf("Przesiadki:") + 12);
        //TODO: export to other method
        String date, day, mounth;
        date = timetable.substring(timetable.indexOf("Data:") + 5);
        if(date.charAt(6)=='.'){
            mounth = date.substring(5, 6);
        }
        else{
            mounth = date.substring(5, 7);
        }
        if(date.substring(date.length()-2).contains(".")){
            day = date.substring(date.length()-1);
        }
        else{
            day = date.substring(date.length()-2);
        }
        String year = date.substring(0, 4);
        String hourArrival = arrival.substring(0, 2);
        String minuteArrival = arrival.substring(3, 5);
        String hourDeparture = departure.substring(0, 2);
        String minuteDeparture = departure.substring(3, 5);
        Date today = new Date(Integer.valueOf(year) - 3800, Integer.valueOf(mounth) - 2, Integer.valueOf(day));

        Time tempTimeFrom = new Time(Integer.valueOf(hourDeparture), Integer.valueOf(minuteDeparture), 0);
        Time tempTimeTo = new Time(Integer.valueOf(hourArrival), Integer.valueOf(minuteArrival), 0);
        ScheduleDayDAO.getInstance().addScheduleDay(new ScheduleDayData("Pkp Katowice - Tychy", today, tempTimeFrom, tempTimeTo, (String)session.getAttribute("userName"),false));

        List<ScheduleDayData> schedules = ScheduleDayDAO.getInstance().getSchedules(today, (String)session.getAttribute("userName"),false);
        List<ScheduleDayData> freeSchedules = ScheduleUtil.getFreeSchedules(schedules, today, (String)session.getAttribute("userName"));

        for(ScheduleDayData scheduleDay : freeSchedules){
            ScheduleDayDAO.getInstance().addScheduleDay(scheduleDay);
        }

        request.setAttribute("schedules", schedules);
        request.setAttribute("freeSchedules", freeSchedules);
        RequestDispatcher view = request.getRequestDispatcher("WEB-INF/view/paulina/FreeTimeFriday.jsp");
        view.forward(request, response);
    }
}
