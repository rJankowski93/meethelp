package com.app.meethelp.servlet.paulina;

import com.app.meethelp.dao.TimetableDAO;
import com.app.meethelp.dto.TimetableDTO;
import com.app.meethelp.util.DateUtil;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Time;
import java.util.Date;
import java.util.List;

public class CheckPkpTychyToWroclawServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String pkpSundayParam = request.getParameter("pkpSunday");
        String dateFridayParam = (String)session.getAttribute("dateFriday");
        Date dateFriday = new Date();
        dateFriday.setDate(Integer.valueOf(dateFridayParam.substring(0, 2)));
        dateFriday.setMonth(Integer.valueOf(dateFridayParam.substring(3, 5)) - 1);
        dateFriday.setYear(Integer.valueOf(dateFridayParam.substring(6, 10)) - 1900);

        Date date = DateUtil.addDays(dateFriday,2);

        Time time = new Time(Integer.valueOf(pkpSundayParam.substring(0, 2)), Integer.valueOf(pkpSundayParam.substring(3, 5)), 0);

        List<TimetableDTO> timetableTychyToWro = TimetableDAO.getInstance().readTimetablePKP("Tychy", "Wroclaw", date.getYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes(), date);

        String jsonObject = new Gson().toJson(timetableTychyToWro);
        response.getWriter().print(jsonObject);
    }
}
