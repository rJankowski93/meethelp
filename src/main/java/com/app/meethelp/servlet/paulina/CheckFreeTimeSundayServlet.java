package com.app.meethelp.servlet.paulina;

import com.app.meethelp.dao.ScheduleDayDAO;
import com.app.meethelp.data.ScheduleDayData;
import com.app.meethelp.util.DateUtil;
import com.app.meethelp.util.ScheduleUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Time;
import java.util.*;

public class CheckFreeTimeSundayServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userName = (String)session.getAttribute("userName");

        //pkp
        String timetable = request.getParameter("timetable");
        String departure = timetable.substring(timetable.indexOf("Odjazd:") + 7, timetable.indexOf("Odjazd:") + 12);
        String arrival = timetable.substring(timetable.indexOf("Przyjazd:") + 9, timetable.indexOf("Przyjazd:") + 14);
        String time = timetable.substring(timetable.indexOf("Czas:") + 5, timetable.indexOf("Czas:") + 9);
        String changes = timetable.substring(timetable.indexOf("Przesiadki:") + 11, timetable.indexOf("Przesiadki:") + 12);
        String hourArrival = arrival.substring(0, 2);
        String minuteArrival = arrival.substring(3, 5);
        String hourDeparture = departure.substring(0, 2);
        String minuteDeparture = departure.substring(3, 5);

        //school and things
        String dateFridayParam = (String)session.getAttribute("dateFriday");
        String endSchoolSundayParam = request.getParameter("endSchoolSunday");
        String pkpSundayParam = request.getParameter("pkpSunday");
        Time endSchoolSunday = new Time(Integer.valueOf(endSchoolSundayParam.substring(0, 2)), Integer.valueOf(endSchoolSundayParam.substring(3, 5)), 0);

        Date dateFriday = new Date();
        dateFriday.setDate(Integer.valueOf(dateFridayParam.substring(0, 2)));
        dateFriday.setMonth(Integer.valueOf(dateFridayParam.substring(3, 5)) - 1);
        dateFriday.setYear(Integer.valueOf(dateFridayParam.substring(6, 10)) - 1900);

        Date dateSunday = DateUtil.addDays(dateFriday,2);

        Time tempTimeFrom= new Time(Integer.valueOf(hourDeparture), Integer.valueOf(minuteDeparture), 0);
        Time tempTimeTo= new Time(Integer.valueOf(hourArrival), Integer.valueOf(minuteArrival), 0);
        ScheduleDayData pkpSunday = new ScheduleDayData("Pkp Tychy - Wroclaw", dateSunday, tempTimeFrom, tempTimeTo, userName, false);
        ScheduleDayDAO.getInstance().addScheduleDay(pkpSunday);


        ScheduleDayData scheduleSunday = new ScheduleDayData("Szkola", dateSunday, new Time(8, 0, 0), endSchoolSunday, userName,false);

        ScheduleDayDAO.getInstance().addScheduleDay(scheduleSunday);

        Map<String, String[]> parameters = new LinkedHashMap<>(request.getParameterMap());
        parameters.remove("dateSunday");
        parameters.remove("endSchoolSunday");
        parameters.remove("pkpSunday");
        List<ScheduleDayData> schedules = ScheduleUtil.getSchedulesFromParams(parameters,dateSunday, userName);
        schedules.add(scheduleSunday);
        schedules.add(pkpSunday);
        Collections.sort(schedules, new ScheduleDayData());

        List<ScheduleDayData> freeSchedules = ScheduleUtil.getFreeSchedules(schedules,dateSunday,userName);
        freeSchedules.remove(freeSchedules.size()-1);

        for(ScheduleDayData scheduleDay : freeSchedules){
            ScheduleDayDAO.getInstance().addScheduleDay(scheduleDay);
        }

        request.setAttribute("schedules", schedules);
        request.setAttribute("freeSchedules", freeSchedules);
        RequestDispatcher view = request.getRequestDispatcher("WEB-INF/view/paulina/FreeTimeSunday.jsp");
        view.forward(request, response);
    }
}
