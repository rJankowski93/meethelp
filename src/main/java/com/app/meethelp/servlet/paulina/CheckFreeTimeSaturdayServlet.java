package com.app.meethelp.servlet.paulina;

import com.app.meethelp.dao.ScheduleDayDAO;
import com.app.meethelp.data.ScheduleDayData;
import com.app.meethelp.util.DateUtil;
import com.app.meethelp.util.ScheduleUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Time;
import java.util.*;

public class CheckFreeTimeSaturdayServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userName = (String)session.getAttribute("userName");

        String dateFridayParam = (String)session.getAttribute("dateFriday");
        String endSchoolSaturdayParam = request.getParameter("endSchoolSaturday");

        Time endSchoolSaturday = new Time(Integer.valueOf(endSchoolSaturdayParam.substring(0, 2)), Integer.valueOf(endSchoolSaturdayParam.substring(3, 5)), 0);

        Date dateFriday = new Date();
        dateFriday.setDate(Integer.valueOf(dateFridayParam.substring(0, 2)));
        dateFriday.setMonth(Integer.valueOf(dateFridayParam.substring(3, 5)) - 1);
        dateFriday.setYear(Integer.valueOf(dateFridayParam.substring(6, 10)) - 1900);

        Date dateSaturday = DateUtil.addDays(dateFriday,1);

        ScheduleDayData scheduleSaturday = new ScheduleDayData("Szkola", dateSaturday, new Time(8, 0, 0), endSchoolSaturday, userName, false);

        ScheduleDayDAO.getInstance().addScheduleDay(scheduleSaturday);

        Map<String, String[]> parameters = new LinkedHashMap<>(request.getParameterMap());
        parameters.remove("dateSaturday");
        parameters.remove("endSchoolSaturday");

        List<ScheduleDayData> schedules = ScheduleUtil.getSchedulesFromParams(parameters,dateSaturday, userName);
        schedules.add(scheduleSaturday);
        Collections.sort(schedules, new ScheduleDayData());
        List<ScheduleDayData> freeSchedules = ScheduleUtil.getFreeSchedules(schedules,dateSaturday,userName);
        for(ScheduleDayData scheduleDay : freeSchedules){
            ScheduleDayDAO.getInstance().addScheduleDay(scheduleDay);
        }


        request.setAttribute("schedules", schedules);
        request.setAttribute("freeSchedules", freeSchedules);
        RequestDispatcher view = request.getRequestDispatcher("WEB-INF/view/paulina/FreeTimeSaturday.jsp");
        view.forward(request, response);
    }
}
