package com.app.meethelp.servlet;

import com.app.meethelp.dao.ScheduleDayDAO;
import com.app.meethelp.data.ScheduleDayData;
import com.app.meethelp.util.DateUtil;
import com.app.meethelp.util.ScheduleUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class FreeWeekendServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dateParam ;
        if(request.getParameter("date") == null){
            dateParam = (String) request.getSession().getAttribute("dateFriday");
        }
        else{
           dateParam = request.getParameter("date");
        }
        String userName = (String) request.getSession().getAttribute("userName");

        String day = dateParam.substring(0, 2);
        String mounth = dateParam.substring(3, 5);
        String year = dateParam.substring(6, 10);

        Date friday = new Date(Integer.valueOf(year) - 1900, Integer.valueOf(mounth) - 1, Integer.valueOf(day));
        Date saturday = DateUtil.addDays(friday, 1);
        Date sunday = DateUtil.addDays(friday, 2);

        List<ScheduleDayData> schedulesFriday = ScheduleDayDAO.getInstance().getSchedules(friday,userName,false);
        List<ScheduleDayData> schedulesSaturday = ScheduleDayDAO.getInstance().getSchedules(saturday,userName,false);
        List<ScheduleDayData> schedulesSunday = ScheduleDayDAO.getInstance().getSchedules(sunday,userName,false);

        List<ScheduleDayData> freeSchedulesFriday = ScheduleUtil.getFreeSchedules(schedulesFriday, friday,userName);
        List<ScheduleDayData> freeSchedulesSaturday = ScheduleUtil.getFreeSchedules(schedulesSaturday, saturday,userName);
        List<ScheduleDayData> freeSchedulesSunday = ScheduleUtil.getFreeSchedules(schedulesSunday, sunday,userName);

        request.setAttribute("schedulesFriday", schedulesFriday);
        request.setAttribute("schedulesSaturday", schedulesSaturday);
        request.setAttribute("schedulesSunday", schedulesSunday);

        request.setAttribute("freeSchedulesFriday", freeSchedulesFriday);
        request.setAttribute("freeSchedulesSaturday", freeSchedulesSaturday);
        request.setAttribute("freeSchedulesSunday", freeSchedulesSunday);

        RequestDispatcher view = request.getRequestDispatcher("WEB-INF/view/FreeTimeWeekend.jsp");
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
