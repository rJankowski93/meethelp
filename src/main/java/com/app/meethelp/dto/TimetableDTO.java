package com.app.meethelp.dto;

import java.util.Date;

public class TimetableDTO {

    private Date date;

    private String departures;

    private String arrivals;

    private String durationTrip;

    private String startPlace;

    private String endPlace;

    private String numberOfChanges;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDepartures() {
        return departures;
    }

    public void setDepartures(String departures) {
        this.departures = departures;
    }

    public String getArrivals() {
        return arrivals;
    }

    public void setArrivals(String arrivals) {
        this.arrivals = arrivals;
    }

    public String getDurationTrip() {
        return durationTrip;
    }

    public void setDurationTrip(String durationTrip) {
        this.durationTrip = durationTrip;
    }

    public String getStartPlace() {
        return startPlace;
    }

    public void setStartPlace(String startPlace) {
        this.startPlace = startPlace;
    }

    public String getEndPlace() {
        return endPlace;
    }

    public void setEndPlace(String endPlace) {
        this.endPlace = endPlace;
    }

    public String getNumberOfChanges() {
        return numberOfChanges;
    }

    public void setNumberOfChanges(String numberOfChanges) {
        this.numberOfChanges = numberOfChanges;
    }
}
