package com.app.meethelp.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ConnectorDB {

    private static ConnectorDB instance;

    private Configuration configuration;
    private SessionFactory sessionFactory;
    public Session session;

    public static final ConnectorDB getInstance() {
        if (instance == null) {
            synchronized (ConnectorDB.class) {
                if (instance == null) {
                    instance = new ConnectorDB();
                }
            }
        }
        return instance;
    }

    private ConnectorDB() {
        configuration = new Configuration().configure();
        sessionFactory = configuration.buildSessionFactory();
        session = sessionFactory.openSession();
    }
}
