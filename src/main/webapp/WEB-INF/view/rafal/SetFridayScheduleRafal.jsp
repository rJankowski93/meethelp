<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Set weekend schedule</title>
    <link rel="stylesheet" href="<c:url value="/resources/front-lib/bootstrap.css"/>">
    <script src="<c:url value="/resources/front-lib/jquery.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/moment.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/combodate.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/style/style.css"/>">
</head>
<body>
<div class="container">
    <h1>Friday</h1>
    <form id="form" method="POST" action="CheckFreeTimeFridayRafal.do">
        <div class="col-md-offset-3 col-md-6" style="margin-top: 40px;">
            <div class="col-md-offset-3 col-md-6">
                <label><b>Data</b></label>
                <input id="date" data-format="DD-MM-YYYY" data-template="D MMM YYYY" name="date" value="09-12-2016" type="text">
            </div>
            <input class="col-md-5" id="nameSchedule" placeholder="Nazwa">
            <input class="btn-danger" type="button" value="Dodaj nowe zajcie" onclick="addNewSchedule()">
        </div>
        <button class="col-md-offset-3 col-md-6" type="submit">Sprawdz wolny czas</button>
    </form>
</div>
</body>
<script>
    $(function () {
        $('#date').combodate({
            minYear: 2015,
            maxYear: 2017
        });
    });
    function addNewSchedule() {
        var name = $('#nameSchedule').val();
        $('#nameSchedule').val("");
        var tempalte = "<div class='col-md-offset-3 col-md-6'>" +
                "<label class='col-xs-12'><b>" + name + "</b></label>" +
                "Godzina rozpoczecia" +
                "<input id='timeFrom" + name + "' class='time' data-format='HH:mm' data-template='HH : mm' name='timeFrom" + name + "' type='text'><br>" +
                "Godzina zakonczenia" +
                "<input id='timeTo" + name + "' class='time' data-format='HH:mm' data-template='HH : mm' name='timeTo" + name + "' type='text'>" +
                "</div>"

        $('#form').append(tempalte);
        $(function () {
            $('.date').combodate({
                minYear: 2015,
                maxYear: 2017
            });
        });
        $(function () {
            $('.time').combodate({
                firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
                minuteStep: 1
            });
        });
    }

</script>
</html>
