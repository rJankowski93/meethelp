<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Choose option</title>
    <link rel="stylesheet" href="<c:url value="/resources/front-lib/bootstrap.css"/>">
    <script src="<c:url value="/resources/front-lib/jquery.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/moment.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/combodate.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/style/style.css"/>">
</head>
<body>
<div class="container">
    <form action="CheckPkpKatowiceToTychy.do" method="GET">
       <c:choose>
           <c:when test="${sessionScope.userName == 'Paulina'}">
               <button id="stepByStep" onclick="form.action='SetUniversityAndSchool.do';"> Krok po kroku</button>
           </c:when>
           <c:when test="${sessionScope.userName == 'Rafal'}">
               <button id="stepByStep" onclick="form.action='StartStepByStepRafal.do';"> Krok po kroku</button>
           </c:when>
       </c:choose>
        <label>Podaj date(jezeli chcesz porownac weekend wybierz date piatku:</label>
        <div class="col-md-offset-3 col-md-6">
            <label><b>Data</b></label>
            <input id="date" data-format="DD-MM-YYYY" data-template="D MMM YYYY" name="date" value="09-12-2016" type="text">
        </div>
        <button id="freeDay" onclick="form.action='FreeDayForBoth.do';"> Wyswietl wolny czas z dnia</button>
        <button id="freeWeekend" onclick="form.action='FreeWeekendForBoth.do';"> Wyswietl wolny czas w weekend</button>
    </form>

</div>
<script>
    $(function () {
        $('#date').combodate({
            minYear: 2015,
            maxYear: 2017
        });
    });
</script>
</body>
</html>
