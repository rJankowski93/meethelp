<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Choose timetable</title>
    <link rel="stylesheet" href="<c:url value="/resources/front-lib/bootstrap.css"/>">
    <script src="<c:url value="/resources/front-lib/jquery.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/moment.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/combodate.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/style/style.css"/>">
</head>
<body>

<div class="container">
    <div class="col-md-offset-2 col-md-8">
        <label><b>Wybierz czas poalczenia: Katowice - Tychy Lodowisko</b></label>
        <form action="CheckFreeTimeFriday.do" method="POST">
            <select name="timetable" class="btn-primary" id="myselect" onchange="this.form.submit()">
                <option value="Wybierz odpowiadajace polaczenie"></option>
                <c:forEach items="${timetableKatoToTychy}" var="timetable">
                    <option style="word-spacing: 20px" class="btn-primary"
                            value="Odjazd:${timetable.departures}Przyjazd:${timetable.arrivals}Czas:${timetable.durationTrip} Przesiadki:${timetable.numberOfChanges}Data:${timetable.date.getYear()+1900}.${timetable.date.getMonth()+1}.${timetable.date.getDate()}">
                        <a href="#">Odjazd:${timetable.departures} Przyjazd:${timetable.arrivals}
                            Czas:${timetable.durationTrip} Przesiadki:${timetable.numberOfChanges} </a></option>
                </c:forEach>
            </select>
        </form>
    </div>
</div>

</body>
<script>
    $('.dropdown-menu a').on('click', function () {
        $('.dropdown-toggle').html($(this).html() + '<span class="caret"></span>');
    })
</script>
</html>
