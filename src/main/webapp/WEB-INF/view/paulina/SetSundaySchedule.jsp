<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Set weekend schedule</title>
    <link rel="stylesheet" href="<c:url value="/resources/front-lib/bootstrap.css"/>">
    <script src="<c:url value="/resources/front-lib/jquery.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/moment.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/combodate.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/style/style.css"/>">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#checkTimetable').click(function () {
                $.ajax({
                    type: "GET",
                    url: "CheckPkpTychyToWroclaw.do", //this is my servlet
                    data: "date=" + $('#dateSunday').val() + "&pkpSunday=" + $('#pkpSunday').val(),
                    success: function (msg) {
                        $("#timetable").find('option').remove();
                        //TODO: zmienic na jQuerry
                        var select = document.getElementById('timetable');
                        select.options[select.options.length] = new Option("Wybierz odpowiadajace polaczenie","Wybierz odpowiadajace polaczenie");
                        var data = jQuery.parseJSON(msg);
                        $.each(data, function (i, item) {
                            select.options[select.options.length] = new Option("Odjazd:" + item.departures + "  Przyjazd:" + item.arrivals + "  Czas:" + item.durationTrip + "  Przesiadki:" + item.numberOfChanges, "Odjazd:" + item.departures + "Przyjazd:" + item.arrivals + "Czas:" + item.durationTrip + "Przesiadki:" + item.numberOfChanges + "Data:" + item.date);
                        })
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });
            });

        });
    </script>
</head>
<body>
<div class="container">
    <form id="form" method="POST" action="CheckFreeTimeSunday.do">
        <div class="col-md-offset-3 col-md-6">
            <label class="col-xs-12"><b>O ktorej konczysz szkole w niedziele?</b></label>
            <input id="endSchoolSunday" class="time" data-format="HH:mm" data-template="HH : mm" name="endSchoolSunday" type="text">
        </div>
        <div class="col-md-offset-3 col-md-6">
            <label class="col-xs-12"><b>O ktorej chcesz jechac do Wroclawia?</b></label>
            <input id="pkpSunday" class="time" data-format="HH:mm" data-template="HH : mm" name="pkpSunday" type="text">
        </div>
        <div id="additionalSchedule" class="col-md-offset-3 col-md-6" style="margin-top: 40px;">
            <input class="col-md-5" id="nameSchedule" placeholder="Nazwa">
            <input class="btn-danger" type="button" value="Dodaj nowe zajcie" onclick="addNewSchedule()">
        </div>
        <button id="checkTimetable" class="col-md-offset-3 col-md-6" type="button">Sprawdz polaczenia</button>
        <select id="timetable" name="timetable" class="btn-primary col-md-offset-3 col-md-6" id="myselect" >
            <option value="Wybierz odpowiadajace polaczenie"></option>
        </select>
        <button class="col-md-offset-3 col-md-6" type="submit">Sprawdz wolny czas</button>
    </form>
</div>

</body>
<script>
    $(function () {
        $('.time').combodate({
            firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
            minuteStep: 1
        });
    });
    $(function () {
        $('.date').combodate({
            minYear: 2015,
            maxYear: 2017
        });
    });
    function addNewSchedule() {
        var name = $('#nameSchedule').val();
        $('#nameSchedule').val("");
        var tempalte = "<div class='col-md-12'>" +
                "<label class='col-xs-12'><b>" + name + "</b></label>" +
                "Godzina rozpoczecia" +
                "<input id='timeFrom" + name + "' class='time' data-format='HH:mm' data-template='HH : mm' name='timeFrom" + name + "' type='text'><br>" +
                "Godzina zakonczenia" +
                "<input id='timeTo" + name + "' class='time' data-format='HH:mm' data-template='HH : mm' name='timeTo" + name + "' type='text'>" +
                "</div>"

        $('#additionalSchedule').append(tempalte);
        $(function () {
            $('.date').combodate({
                minYear: 2015,
                maxYear: 2017
            });
        });
        $(function () {
            $('.time').combodate({
                firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
                minuteStep: 1
            });
        });
    }
</script>
</html>