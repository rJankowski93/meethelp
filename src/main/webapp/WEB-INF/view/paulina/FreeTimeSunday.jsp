<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ScheduleDay - Sunday</title>
    <link rel="stylesheet" href="<c:url value="/resources/front-lib/bootstrap.css"/>">
    <script src="<c:url value="/resources/front-lib/jquery.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/bootstrap.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/style/style.css"/>">
</head>
<body>
<table class="table">
    <h1>Twoje rzeczy w niedziele:</h1>
    <tr>
        <th>Co?</th>
        <th>Godzina rozpoczecie</th>
        <th>Godzina zakonczenia</th>
    </tr>
    <c:forEach items="${schedules}" var="schedule">
        <tr>
            <td>
                <c:out value="${schedule.name}    "/>
            </td>
            <td>
                <c:out value="${schedule.dateFrom}"/>
            </td>
            <td>
                <c:out value="${schedule.dateTo}"/>
            </td>
        </tr>
    </c:forEach>
</table>

<table class="table">
    <h1>Twoje przerwy w niedziele:</h1>
    <tr>
        <th>Co?</th>
        <th>Godzina rozpoczecie</th>
        <th>Godzina zakonczenia</th>
    </tr>
    <c:forEach items="${freeSchedules}" var="schedule">
        <tr>
            <td>
                <c:out value="${schedule.name}"/>
            </td>
            <td>
                <c:out value="${schedule.dateFrom}"/>
            </td>
            <td>
                <c:out value="${schedule.dateTo}"/>
            </td>
        </tr>
    </c:forEach>
</table>
<div class="container">
    <form action="FreeWeekend.do" method="GET">
        <button>Podsumowanie</button>
    </form>
</div>
</body>
</html>
