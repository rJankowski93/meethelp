<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Set university & school</title>
    <link rel="stylesheet" href="<c:url value="/resources/front-lib/bootstrap.css"/>">
    <script src="<c:url value="/resources/front-lib/jquery.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/moment.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/combodate.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/style/style.css"/>">
</head>
<body>
<div class="container">
    <form method="POST" action="CheckPkpWroclawToKatowice.do">
        <div class="col-md-offset-3 col-md-6">
            <label><b>Data</b></label>
            <input id="date" data-format="DD-MM-YYYY" data-template="D MMM YYYY" name="date" value="09-12-2016" type="text">
        </div>
        <div class="col-md-offset-3 col-md-6">
            <label><b>O ktorej konczysz uczelnie w piatek?</b></label>
            <input id="timeUniversity" class="time" data-format="HH:mm" data-template="HH : mm" name="timeUniversity" type="text">
        </div>
        <div class="col-md-offset-3 col-md-6">
            <label><b>O ktorej zaczynasz szkole w piatek?</b></label>
            <input id="startSchool" class="time" data-format="HH:mm" data-template="HH : mm" name="startSchool" type="text">
        </div>
        <div class="col-md-offset-3 col-md-6">
            <label><b>O ktorej kończysz szkole w piatek?</b></label>
            <input id="endSchool" class="time" data-format="HH:mm" data-template="HH : mm" name="endSchool" type="text">
        </div>
        <button class="col-md-offset-3 col-md-6" type="submit">Sprawdz polaczenia</button>
    </form>
</div>
</body>
<script>
    $(function () {
        $('.time').combodate({
            firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
            minuteStep: 1
        });
    });
    $(function () {
        $('#date').combodate({
            minYear: 2015,
            maxYear: 2017
        });
    });
</script>
</html>
