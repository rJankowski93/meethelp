<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="<c:url value="/resources/front-lib/bootstrap.css"/>">
    <script src="<c:url value="/resources/front-lib/jquery.js"/>"></script>
    <script src="<c:url value="/resources/front-lib/bootstrap.js"/>"></script>
    <link rel="stylesheet" href='<c:url value='/resources/style/style.css'/>'>
</head>
<body>
<form method="POST" action="Login.do">
    <div class="imgcontainer">
        <img src="/resources/img/img_avatar2.png" alt="Avatar" class="avatar">
    </div>

    <div class="col-lg-offset-1">
        <label><b>Login</b></label>
        <input type="text" placeholder="Wpisz login" name="name" required>

        <label><b>Haslo</b></label>
        <input type="password" placeholder="Wpisz haslo" name="password" required>

        <button type="submit">Login</button>
    </div>
</form>
</body>
</html>